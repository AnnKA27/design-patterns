package Factory.pizzas.pizzaStores;


import Factory.ingridients.IngridientFactories.NYPizzaIngridientFactory;
import Factory.ingridients.PizzaIngredientFactory;
import Factory.pizzas.*;

public class NYPizzaStore extends PizzaStore {
    PizzaIngredientFactory ingredientFactory;

    public NYPizzaStore() {
        this.ingredientFactory = new NYPizzaIngridientFactory();
    }

    @Override
    Pizza createPizza(String type) {
        Pizza pizza = null;

        if (type.equals("cheese")) {
            pizza = new CheesePizza(ingredientFactory);
            pizza.setName("NYP CheesePizza");
        } else if (type.equals("pepperoni")) {
            pizza = new PepperoniPizza(ingredientFactory);
            pizza.setName("NYP Pepperoni Pizza");
        } else if (type.equals("clam")) {
            pizza = new ClamPizza(ingredientFactory);
            pizza.setName("NYP Clam Pizza");
        } else if (type.equals("veggie")) {
            System.out.println("NYP Veggie Pizza");
            pizza = new VeggiePizza(ingredientFactory);
            pizza.setName("NYP Veggie Pizza");
        }
        return pizza;
    }
}
