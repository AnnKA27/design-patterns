package Factory;

import Factory.pizzas.Pizza;
import Factory.pizzas.pizzaStores.ChicagoPizzaStore;
import Factory.pizzas.pizzaStores.NYPizzaStore;
import Factory.pizzas.pizzaStores.PizzaStore;

public class Main {
    public static void main(String[] args) {
        PizzaStore nyPizzaStore = new NYPizzaStore();
        Pizza pizza = nyPizzaStore.orderPizza("cheese");
        System.out.println(pizza.toString());


        PizzaStore  chicagoPizzaStore = new ChicagoPizzaStore();
         pizza = chicagoPizzaStore.orderPizza("cheese");
        System.out.println(pizza.toString());
    }
}
