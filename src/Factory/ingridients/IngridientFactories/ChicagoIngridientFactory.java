package Factory.ingridients.IngridientFactories;

import Factory.ingridients.*;

public class ChicagoIngridientFactory implements PizzaIngredientFactory {

    @Override
    public Dough createDough() {
        return new Dough();
    }

    @Override
    public Sauce createSauce() {
        return new MarinaraSauce();
    }

    @Override
    public Cheese createCheese() {
        return new MaasdamCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        return new Veggies[]{new Eggplant(), new Garlic()};
    }

    @Override
    public Pepperoni createPepperoni() {
        return new Pepperoni();
    }

    @Override
    public Clams createClam() {
        return new Clams();
    }
}
